<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Tropic extends Model 
{
    protected $table = "tropics";
    protected $fillable = [
        'user_id', 'tropic_id', 'group_id', 'type',  'status', 
    ];
	
	public function tropic()
    {
        return $this->hasOne('App\AllTropic','id','tropic_id');
    }
}
