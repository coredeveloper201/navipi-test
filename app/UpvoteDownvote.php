<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class UpvoteDownvote extends Model 
{
    protected $table = "upvote_downvotes";
    protected $fillable = [
        'answer_id', 'user_id', 'status'
    ];

}
