<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class TropicQuestion extends Model 
{
    protected $table = "tropic_questions";
    protected $fillable = [
        'user_id', 'tropic_id', 'question_id',
    ];
	
	public function tropic()
    {
        return $this->hasOne('App\AllTropic','id','tropic_id');
    }
	public function question()
    {
        return $this->hasOne('App\AskQuestion','id','question_id');
    }
}
