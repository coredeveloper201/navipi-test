<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class TestEvaluation extends Model 
{
	protected $fillable = [
        'first_name','last_name','email_address','test_id','user_id','score','quiz_attends','status'
    ];
	
	public function test_answers()
    {
        return $this->hasMany('App\TestAnswer','test_eavl_id','id');
    }
	
	
}
