<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ContactMessages extends Model
{
	protected $table='contact_messages';
   
    protected $fillable = ['sender_name', 'sender_email', 'phone', 'comments'];
	
}
