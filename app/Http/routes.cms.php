<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// could be page/{slug} or only slug
Route::group(['middleware' => 'locale'], function() {
	Route::get('/{slug}', array('as' => 'page.show', 'uses' => 'PageController@show'));
});








