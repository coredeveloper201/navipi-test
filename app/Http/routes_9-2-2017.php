<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('topics', 'HomeController@topics');
Route::get('register-as', 'HomeController@registerAs');
Route::get('register/{type}', 'Auth\AuthController@showRegistrationForm');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('register/verify/{code}/{utype}', 'UserController@activate');
Route::get('pricing', 'HomeController@accountUpgrade');
Route::post('pricing', 'HomeController@accountUpgrade');
Route::get('contact-us', 'HomeController@contactUs');
Route::post('contact-us', 'HomeController@contactProcess');
Route::post('get-package', 'HomeController@getPackage');
Route::post('social-login', 'Auth\AuthController@socialLogin');

/*---------------------- Forgot Password Section -----------------------*/
Route::get('forget-password','ForgotPasswordController@forgetPassword');
Route::post('forget-password','ForgotPasswordController@forgotPasswordProcess');
Route::get('reset-password/{token}','ForgotPasswordController@resetPassword');
Route::get('reset-password','ForgotPasswordController@resetPassword');
Route::post('reset-password','ForgotPasswordController@resetPassProcess');
/*---------------------- Forgot Password Section -----------------------*/

Route::get('profile/{id}', 'UserController@account');

Route::group(['middleware' => 'auth'], function() {
	Route::group(['prefix' => 'user'], function() {
		Route::get('signup-step/{step}', 'UserController@signupStep');
		Route::post('signup-step/{step}', 'UserController@signupStepProcess');
		Route::get('account', 'UserController@account');
		Route::post('add-address/', 'UserController@addAddress');
		Route::post('add-workexp/', 'UserController@addWorkExpProcess');
		Route::post('add-education/', 'UserController@addEducationProcess');
		Route::post('add-tropic/', 'UserController@addTropicProcess');
		Route::post('del-workexp', 'UserController@delWorkExpProcess');
 		Route::post('del-education', 'UserController@delEducationProcess');
  		Route::post('del-tropic', 'UserController@delTropicProcess');
  		Route::post('del-address', 'UserController@delAddress');
		Route::post('update-userdata', 'UserController@userUpdateProcess');
		Route::get('setting', 'UserController@setting');
		Route::get('privacy', 'UserController@privacy');
		Route::get('notification', 'UserController@notification');
		Route::post('update-privacy-settings', 'UserController@updatePrivacySettings');
		Route::post('update-notify-settings', 'UserController@updateNotificationSettings');
		Route::post('setdeflt-address', 'UserController@setDefaultAddress');
		
		/* Route::get('pay-notify/{id}/{user_id}', 'UserController@paymentNotify');
		Route::post('pay-notify', 'UserController@paymentPostNotify');
		Route::get('upgrade', 'UserController@accountUpgrade');
		Route::post('upgrade', 'UserController@accountUpgradeProcess');
		Route::get('my-details', 'UserController@myDetails');
		Route::post('update-account', 'UserController@updateAccount'); */
		Route::post('add-favorite', 'UserController@addFavorite');
		Route::get('show-advance-search', 'UserController@showAdvanceSearch');
		Route::post('post-question', 'UserController@postQuestion');
		Route::post('add-contactinfo', 'UserController@addContactinfo');
		Route::post('del-contactinfo', 'UserController@deleteContactinfo');
		Route::get('show-askqsn', 'UserController@showAskqsn');
		Route::post('set-filter', 'UserController@setFilter');
	});
});
		Route::get('user/search-people', 'UserController@searchPeople');
		Route::post('user/search-users', 'UserController@searchUsers');
		Route::get('user/company-autocomplete', 'UserController@getCompanyAutocomplete');
		Route::get('user/job-autocomplete', 'UserController@getJobAutocomplete');
		Route::get('user/university-autocomplete', 'UserController@getUniversityAutocomplete');
		Route::get('user/major-autocomplete', 'UserController@getSchoolMajorAutocomplete');
		Route::get('clear-cache',function(){
			Cache::flush();
		});
Route::group(['middleware' => 'locale'], function() {
	Route::get('sl/{locale}', function ($locale) {
	});
	Route::get('/', 'HomeController@index');
});