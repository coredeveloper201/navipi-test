<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	/* protected function resetEmailBuilder()
    {
		
        return function (Message $message) {
			$setting = app('settings');
            $message->subject($this->getEmailSubject());
            $message->from($setting->contact_email, $setting->contact_name);
        };
    } */
	/* public function postEmail(Request $request)
	{
		$this->validate($request, ['email' => 'required']);
		
		$response = $this->passwords->sendResetLink($request->only('email'), function($message)
		{
			$setting = app('settings');
			$message->subject('Password Reminder');
			$message->from($setting->contact_email, $setting->contact_name);
		});

		switch ($response)
		{
			case PasswordBroker::RESET_LINK_SENT:
				return redirect()->back()->with('status', trans($response));

			case PasswordBroker::INVALID_USER:
				return redirect()->back()->withErrors(['email' => trans($response)]);
		}
	} */
	/* public function sendResetLinkEmail(Request $request)
    {

        $this->validate($request, ['email' => 'required|email']);

        $broker = $this->getBroker();
		$setting = app('settings');
        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
            $message->from($setting->contact_email, $setting->contact_name);
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);

            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    } */
	
}
