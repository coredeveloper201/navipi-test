<?php



namespace App\Http\Controllers\Admin;


use Image;
use App\AllTropic;
use App\TropicGroup;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class TropicController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')

    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $tropics = AllTropic::orderBy('id', 'desc')->paginate($per_page);

        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.tropics.list', compact('tropics','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        return view('admin.tropics.add');

    }
	
    protected function csvValidator($file)
    {
		return $validator = Validator::make(
				[
					'file'      => $file,
					'extension' => strtolower($file->getClientOriginalExtension()),
				],
				[
					'file'          => 'required',
					'extension'      => 'required|in:csv',
				],
				[
					'extension.in' => 'Please upload a valid CSV file'
				]
			);
    }
    public function addCsv()
    {		
		$allTropic = AllTropic::all();
		/* dd($allTropic->toArray());
		$list = array (); */
		$list = array (	
			array('TOPIC', 'GROUP'),
			array('', ''),
		);
		if(!empty($allTropic))
		{
			foreach($allTropic as $val){
				$list[]=array($val->title, $val->group['name']);
			}
		}
		$file = fopen("assets/upload/topic_csv/tropics.csv","w");

		foreach ($list as $line)
		{
			fputcsv($file,$line);
		}

		fclose($file);
        return view('admin.tropics.add-csv');
    }

	public function uploadCsv(Request $request)
    {
        $this->validate($request, [
                'topic_csv' => 'required',
            ]
        );
		
		$inp_data=$request->all();
		//dd($inp_data);
		
		if ($request->hasFile('topic_csv')){

			$file = $request->file('topic_csv');
			$validator = $this->csvValidator($file);

			if ($validator->fails()) {
				$this->throwValidationException(
					$request, $validator
				);
			}
			
			$name = time() . '-' . $file->getClientOriginalName();

			//check out the edit content on bottom of my answer for details on $storage
			$storage = public_path(); 
			$path = $storage . '/assets/upload/topic_csv/';

			// Moves file to folder on server
			$file->move($path, $name);
			//echo $name;
			// Import the moved file to DB and return OK if there were rows affected
			//echo $updb = ( $this->_import_csv($path, $name) ? 'OK' : 'No rows affected' ); 
			$old_file = 'assets/upload/topic_csv/'.$name;
			$row = 1;
			if (($handle = fopen($old_file, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$num = count($data);
					//echo "<p> $num fields in line $row: <br /></p>\n";
					
					for ($c=0; $c < $num; $c++) {
						/* echo $data[$c] . "<br />\n"; */
						if($c==0)	$title = $data[$c];
						if($c==1 && $row >2)
						{
							$grup = $data[$c];
							if(trim($grup)!='')
							{
								$tgrp = TropicGroup::where('name',strtolower($grup))->first();
								if(empty($tgrp))
								{
									$tgrp = TropicGroup::create(array('name'=>$grup));				
								}
								$group_id = $tgrp->id;
							}
							else	$group_id = 0;
						}
						
						if($c==2){
							return redirect()->back()->with('error', 'Inappropiate CSV file.');
							break;
						}
					}
					if($row >2){
						$fdata = AllTropic::where('title',$title)->first();
						if(empty($fdata))
							AllTropic::create(['title'=>$title,'group_id'=>$group_id]);
					}
					$row++;
				}
				fclose($handle);
			}
			
            \File::delete($old_file);
			return redirect()->back()->with('success', 'Tropic successfully added.');
		 }
		return redirect()->back()->with('error', 'Tropic add error.');
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(Request $request)

    {		
        $this->validate($request, [

                'title' => 'required|max:255',
            ]
        );
		$inp_data=$request->all();
		//dd($inp_data);
		$title = $inp_data['title'];
		
		$title_arr = explode('(',$inp_data['title']);
		//dd($title_arr);
		if(isset($title_arr[1]))
		{
			$grup = preg_replace("/[^a-zA-Z0-9]/", "", trim($title_arr[1]));
			$tgrp = TropicGroup::where('name',strtolower($grup))->first();
			//dd($tgrp->toArray());
			if(empty($tgrp))
			{
				$tgrp = TropicGroup::create(array('name'=>$grup));				
			}
			$data['group_id']= $tgrp->id;
		}else	$data['group_id']= 0;
			
		
		
		$data['title']= $title_arr[0];
		$time = time();
		 if($request->hasFile('image')){
            /* $old_image = 'assets/upload/topic/'.$settings->image;
            \File::delete($old_image); */

            $path   = public_path().'/assets/upload/topic/';
            $image  = $request->file('image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $data['image'] = $save_name;
        }

        AllTropic::create($data);

        return redirect()->back()->with('success', 'Tropic successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $tropic = AllTropic::find($id);
        $groups = TropicGroup::all();
		
		//dd($packages->toArray());

        return view('admin.tropics.edit', compact('tropic','groups'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {

        $this->validate($request, [

                'title' => 'required|max:255',
                'group_id' => 'required',

            ]

        );
		$inp_data=$request->all();
		$data['title']= $inp_data['title'];
		$data['group_id']= $inp_data['group_id'];
        $tropic = AllTropic::find($id);
		
		$time = time();
		 if($request->hasFile('image')){
            $old_image = 'assets/upload/topic/'.$tropic->image;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/topic/';
            $image  = $request->file('image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $data['image'] = $save_name;
        }

		\App\Tropic::where('group_id',$tropic->group_id)->update(array('group_id'=>$inp_data['group_id']));
		//unset($data['_method']);unset($data['_token']);
		//dd($data);
        $tropic->fill($data)->save();
        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

        AllTropic::destroy($id);
		\App\Tropic::where('tropic_id',$id)->delete();
        return redirect()->back()->with('success', 'Tropic successfully removed!');

    }
    public function deleteAll(Request $request)
    {
		$input_data = $request->all();
		if(isset($input_data['data_ids']) && $input_data['data_ids']!='')
		{			
		$data_ids=explode(',',$input_data['data_ids']);
		//dd($data_ids);
		
		$d= AllTropic::whereIn('id',$data_ids)->delete();
		$d= \App\Tropic::whereIn('tropic_id',$data_ids)->delete();
		return 1;
        //return redirect()->back()->with('success', 'Records successfully removed!');
		}
		else return 0;
    }



    /* public function deleteTranslation($locale = '', $id)

    {

        if($locale && in_array($locale, config('translatable.locales'))) {

            if($locale == '' || !in_array($locale, config('translatable.locales'))) {

                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

            }

            app()->setLocale($locale);

            $cms = Page::find($id)->getTranslation($locale);

            $cms->delete();

            $current_language = Language::current();

            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');

        }

        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

    } */

}

