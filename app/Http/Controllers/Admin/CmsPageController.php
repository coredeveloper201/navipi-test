<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Language;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CmsPageController extends Controller
{
    protected $languages;
    protected $fallback_language;

    /**
     * CmsPageController constructor.
     */
    public function __construct()
    {
        $this->languages = app('languages');
        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($locale = '')
    {
        if($locale && in_array($locale, config('translatable.locales'))) {
            app()->setLocale($locale);
        }
        $current_language = Language::current();

        $per_page = config('constants.ADMIN_PER_PAGE');
        $cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);
        return view('admin.cms.list', compact('cms', 'current_language'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cms.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//dd($request->toArray());
        $title_field = 'title_' . $this->fallback_language->locale;
        $page_content_field = 'page_content_' . $this->fallback_language->locale;

        $this->validate($request, [
            $title_field  => 'required|max:255',
        ],[
            $title_field . '.required'  => 'The title ('.$this->fallback_language->name.') field is required.',
            $title_field . '.max'  => 'The title ('.$this->fallback_language->name.') may not be greater than 255 characters.',
        ]);
       /*  $this->validate($request, [
            $page_content_field  => 'required',
        ],[
            $page_content_field . '.required'  => 'The Content ('.$this->fallback_language->name.') field is required.',
        ]); */

        $input = $request->all();
		//print_r($input);exit;
        $data = [];
        foreach($this->languages as $language) {
            if(!empty($input['title_' . $language->locale]) ) {
				$str = preg_replace('/[^A-Za-z0-9\. -]/', '', $input['title_' . $this->fallback_language->locale]);
				$slug=strtolower(str_replace(' ','-',trim($str)));
				$slug=strtolower(str_replace('--','-',$slug));
                $data[$language->locale] = ['title' => $input['title_' . $language->locale], 'page_content' => $input['page_content_' . $language->locale], 'slug' =>$slug, 'seo_title' => $input['seo_title_' . $language->locale], 'seo_keywords' => $input['seo_keywords_' . $language->locale] , 'seo_description' => $input['seo_description_' . $language->locale] ];
            }           
        }
		//print_r($data);exit;
        $cms = Page::create($data);
        return redirect()->back()->with('success', 'Page successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cms = Page::find($id);
		//dd($cms->toArray());
        return view('admin.cms.edit', compact('cms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title_field = 'title_' . $this->fallback_language->locale;
		$page_content_field = 'page_content_' . $this->fallback_language->locale;
        $this->validate($request, [
            $title_field  => 'required|max:255',
        ],[
            $title_field . '.required'  => 'The title ('.$this->fallback_language->name.') field is required.',
            $title_field . '.max'  => 'The title ('.$this->fallback_language->name.') may not be greater than 255 characters.',
        ]);
       /*  $this->validate($request, [
            $page_content_field  => 'required',
        ],[
            $page_content_field . '.required'  => 'The Content ('.$this->fallback_language->name.') field is required.',
        ]); */

        $input = $request->all();
		//print_r($input);exit;
        $data = [];
        foreach($this->languages as $language) {
            if(!empty($input['title_' . $language->locale])) {
				$str = preg_replace('/[^A-Za-z0-9\. -]/', '', $input['title_' . $this->fallback_language->locale]);
				$slug=strtolower(str_replace(' ','-',trim($str)));
				$slug=strtolower(str_replace('--','-',$slug));
                $data[$language->locale] = [ 'title' => $input['title_' . $language->locale], 'slug' =>$slug , 'seo_title' => $input['seo_title_' . $language->locale], 'seo_keywords' => $input['seo_keywords_' . $language->locale] ,'seo_description' => $input['seo_description_' . $language->locale] ];
				if(!empty($input['page_content_' . $language->locale]))
				{
					$data[$language->locale]['page_content'] =  $input['page_content_' . $language->locale];
				}
            }           
        }
        $cms = Page::find($id);
        $cms->fill($data)->save();
        return redirect()->back()->with('success', 'Update was successfully done.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        return redirect()->back()->with('success', 'Page successfully removed!');

    }

    public function deleteTranslation($locale = '', $id)
    {
        if($locale && in_array($locale, config('translatable.locales'))) {
            if($locale == '' || !in_array($locale, config('translatable.locales'))) {
                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');
            }
            app()->setLocale($locale);
            $cms = Page::find($id)->getTranslation($locale);
            $cms->delete();
            $current_language = Language::current();
            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');
        }
        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');
    }
	
}
