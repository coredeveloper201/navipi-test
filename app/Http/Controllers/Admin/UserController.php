<?php
namespace App\Http\Controllers\Admin;

use App\User;

use App\Package;

use App\PackageUser;

use App\Invoice;

use Illuminate\Support\Facades\Mail;

use App\EmailTemplate;
use URL;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $per_page = config('constants.ADMIN_PER_PAGE');

        $users = User::orderBy('id', 'desc')->paginate($per_page);

        return view('admin.user.list', compact('users'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', compact('user'));
    }
 
    /**
 
     * Update the specified resource in storage.
 
     *
 
     * @param  \Illuminate\Http\Request  $request
 
     * @param  int  $id
 
     * @return \Illuminate\Http\Response
 
     */
 
    public function update(Request $request, $id)
    {

        $this->validate($request, [



                'first_name' => 'required|max:255',



                'last_name' => 'required|max:255',



                'email' => 'required|email|max:255|unique:users,email,'.$id,



                'address' => 'required|max:255',



            ]



        );
 
        $user = User::find($id);
 
        $user->fill($request->all());
 
        $user->save();
 
        return redirect()->back()->with('success', 'Update was successfully done.');
 
    }
 


    /**
 
     * Remove the specified resource from storage.
 
     *
 
     * @param  int  $id
 
     * @return \Illuminate\Http\Response
 
     */
 
    public function destroy($id)

    {



        /* User::destroy($id);
 
        return redirect()->back()->with('success', 'User successfully removed!'); */

    }

	
    public function block(Request $request)
    {
		$post_data = $request->all();
	    $user = User::find($post_data['uid']);
		/* dd($user); */
		if($user->status=="Y"){

			$status='N';

		}else	$status='Y';


	   User::where('id',$post_data['uid'])->update(['status' => $status]);

       return redirect()->back()->with('success', 'User disabled!');

    }

	
    public function deleteAll(Request $request)
    {
		$input_data = $request->all();
		if(isset($input_data['data_ids']) && $input_data['data_ids']!='')
		{		
			$data_ids=explode(',',$input_data['data_ids']);
			//dd($data_ids);			
			$d= User::whereIn('id',$data_ids)->delete();
			return 1;
			//return redirect()->back()->with('success', 'Records successfully removed!');
		}
		else return 0;
    }


	

}
 
