<?php



namespace App\Http\Controllers\Admin;


use Image;
use App\Blog;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class BlogController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();
		
    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')
    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $blogs = Blog::where('user_id','<>',0)->orderBy('id', 'desc')->paginate($per_page);
        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.blog.list', compact('blogs','current_language'));

    }
    public function adminBlogList()
    {
        $per_page = config('constants.ADMIN_PER_PAGE');
        $blogs = Blog::where('user_id',0)->orderBy('id', 'desc')->paginate($per_page);
        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.blog.list', compact('blogs','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        $categories = Blog::orderBy('title', 'asc')->get();

        return view('admin.blog.add',compact('categories'));

    }

	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(Request $request)
    {		
        $this->validate($request, [

                'title' => 'required|max:255',
                'description' => 'required',
            ]
        );
		$inp_data=$request->all();
		//dd($inp_data);
		$title = trim($inp_data['title']);
		$time = time();
		if($request->hasFile('blog_image')){
            $path   = public_path().'/assets/upload/blog_image/';
            $image  = $request->file('blog_image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $data['blog_image'] = $save_name;
        }
		$data['title']= $title;
		$data['description']= $inp_data['description'];
		$data['user_id']= 0;
        Blog::create($data);

        return redirect()->back()->with('success', 'Blog successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

       $blog = Blog::find($id);
		
		//dd($packages->toArray());

        return view('admin.blog.edit', compact('blog'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {
$this->validate($request, [

                'title' => 'required|max:255',
                'description' => 'required',
            ]
        );
		$inp_data=$request->all();
		//dd($inp_data);
		$title = trim($inp_data['title']);
		$time = time();		
		
		$blog = Blog::find($id);
		$time = time();
		if($request->hasFile('blog_image')){
            $old_image = 'assets/upload/blog_image/'.$blog->blog_image;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/blog_image/';
            $image  = $request->file('blog_image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $blog->blog_image = $save_name;
        }
		
		$data['title']= $title;
		$data['description']= $inp_data['description'];
		//dd($data);
        $blog->fill($data)->save();
        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

       $blog =  Blog::find($id);
        $old_image = 'assets/upload/blog_image/'.$blog->blog_image;
		if(file_exists($old_image)){
            \File::delete($old_image);
		}
       $blog =  Blog::destroy($id);
        return redirect()->back()->with('success', 'Tropic successfully removed!');

    }


    public function block(Request $request)
    {
		$post_data = $request->all();
	    $user = Blog::find($post_data['id']);
		/* dd($user); */
		if($user->status=="Y"){

			$status='N';

		}else	$status='Y';


	   Blog::where('id',$post_data['id'])->update(['status' => $status]);

       return redirect()->back()->with('success', 'Blog disabled!');

    }

    public function deleteAll(Request $request)
    {
		$input_data = $request->all();
		if(isset($input_data['data_ids']) && $input_data['data_ids']!='')
		{		
			$data_ids=explode(',',$input_data['data_ids']);
			//dd($data_ids);			
			$d= Blog::whereIn('id',$data_ids)->delete();
			return 1;
			//return redirect()->back()->with('success', 'Records successfully removed!');
		}
		else return 0;
    }
    /* public function deleteTranslation($locale = '', $id)

    {

        if($locale && in_array($locale, config('translatable.locales'))) {

            if($locale == '' || !in_array($locale, config('translatable.locales'))) {

                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

            }

            app()->setLocale($locale);

            $cms = Page::find($id)->getTranslation($locale);

            $cms->delete();

            $current_language = Language::current();

            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');

        }

        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

    } */

}

