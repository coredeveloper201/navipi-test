<?php



namespace App\Http\Controllers\Admin;



use App\Company;
use App\Job;
use App\University;
use App\SchoolMajor;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class CsvController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')

    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
       /*  $tropics = AllTropic::orderBy('id', 'desc')->paginate($per_page);

        return view('admin.tropics.list', compact('tropics','current_language')); */

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        return;

    }
	
    protected function csvValidator($file)
    {
		return $validator = Validator::make(
				[
					'file'      => $file,
					'extension' => strtolower($file->getClientOriginalExtension()),
				],
				[
					'file'          => 'required',
					'extension'      => 'required|in:csv',
				],
				[
					'extension.in' => 'Please upload a valid CSV file'
				]
			);
    }
    public function manageCsv(Request $request)
    {				
		$last_segment =$request->segment(3);		
		
		switch ($last_segment) {
			case 'company-csv':
					$alldata = Company::all();
				break;
			case 'job-csv':
					$alldata = Job::all();
				break;
			case 'school-csv':
					$alldata = University::all();
				break;
			case 'major-csv':
					$alldata = SchoolMajor::all();
				break;
				
			default: '';
		}
		/* dd($allTropic->toArray());
		$list = array (); */
		$list = array (	
			array('TITLE'),
			array(''),
		);
		if(!empty($alldata))
		{
			foreach($alldata as $val){
				$list[]=array($val->title);
			}
		}
		$file = fopen("assets/upload/CSV/export-list.csv","w");

		foreach ($list as $line)
		{
			fputcsv($file,$line);
		}

		fclose($file); /* */
        return view('admin.csv.manage-csv');
    }

	public function uploadCsv(Request $request)
    {
		$last_segment =$request->segment(3);
        $this->validate($request, [
                'file_csv' => 'required',
            ]
        );
		
		$inp_data=$request->all();
		//dd($inp_data);
		
		if ($request->hasFile('file_csv')){

			$file = $request->file('file_csv');
			$validator = $this->csvValidator($file);

			if ($validator->fails()) {
				$this->throwValidationException(
					$request, $validator
				);
			}
			
			$name = time() . '-' . $file->getClientOriginalName();

			//check out the edit content on bottom of my answer for details on $storage
			$storage = public_path(); 
			$path = $storage . '/assets/upload/CSV/';

			// Moves file to folder on server
			$file->move($path, $name);
			//echo $name;
			// Import the moved file to DB and return OK if there were rows affected
			
			$old_file = 'assets/upload/CSV/'.$name;
			$row = 1;
			if (($handle = fopen($old_file, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$num = count($data);
					//echo "<p> $num fields in line $row: <br /></p>\n";
					
					for ($c=0; $c < $num; $c++) {
						/* echo $data[$c] . "<br />\n"; */
						if($c==0)	$title = $data[$c];
						if($c==1){
							return redirect()->back()->with('error', 'Inappropiate CSV file.');
							break;
						}
					}
					if($row >2)
					{
						switch ($last_segment) 
						{
							case 'company-csv':
									$fdata = Company::where('title',$title)->first();
									if(empty($fdata))
										Company::create(['title'=>$title]);
								break;
							case 'job-csv':
									$fdata = Job::where('title',$title)->first();
									if(empty($fdata))
										Job::create(['title'=>$title]);
								break;
							case 'school-csv':
									$fdata = University::where('title',$title)->first();
									if(empty($fdata))
										University::create(['title'=>$title]);
								break;
							case 'major-csv':
									$fdata = SchoolMajor::where('title',$title)->first();
									if(empty($fdata))
										SchoolMajor::create(['title'=>$title]);
								break;
								
							default: '';
						}						
					}
					$row++;
				}
				fclose($handle);
			}
			
            \File::delete($old_file);
			return redirect()->back()->with('success', 'CSV successfully uploaded.');
		 }
		return redirect()->back()->with('error', 'CSV upload error.');
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(Request $request)
    {		
        return;
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

    }


}

