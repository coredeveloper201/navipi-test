<?php

namespace App\Http\Controllers\Admin;

use App\PhoneType;
use App\Language;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PhoneTypeController extends Controller
{
    protected $languages;
    protected $fallback_language;

    /**
     * PhoneTypeController constructor.
     */
    public function __construct()
    {
        $this->languages = app('languages');
        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($locale = '')
    {
        if($locale && in_array($locale, config('translatable.locales'))) {
            app()->setLocale($locale);
        }
        $current_language = Language::current();

        $per_page = config('constants.ADMIN_PER_PAGE');
        $phone_types = PhoneType::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);
        return view('admin.phone_type.list', compact('phone_types', 'current_language'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.phone_type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name_field = 'name_' . $this->fallback_language->locale;

        $this->validate($request, [
            $name_field  => 'required|max:255',
        ],[
            $name_field . '.required'  => 'The name ('.$this->fallback_language->name.') field is required.',
            $name_field . '.max'  => 'The name ('.$this->fallback_language->name.') may not be greater than 255 characters.',
        ]);

        $input = $request->all();
        $data = [];
        foreach($this->languages as $language) {
            if(!empty($input['name_' . $language->locale])) {
                $data[$language->locale] = ['name' => $input['name_' . $language->locale]];
            }
        }
        $phone_type = PhoneType::create($data);
        return redirect()->back()->with('success', 'Phone type successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone_type = PhoneType::find($id);
        return view('admin.phone_type.edit', compact('phone_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name_field = 'name_' . $this->fallback_language->locale;

        $this->validate($request, [
            $name_field  => 'required|max:255',
        ],[
            $name_field . '.required'  => 'The name ('.$this->fallback_language->name.') field is required.',
            $name_field . '.max'  => 'The name ('.$this->fallback_language->name.') may not be greater than 255 characters.',
        ]);

        $input = $request->all();
        $data = [];
        foreach($this->languages as $language) {
            if(!empty($input['name_' . $language->locale])) {
                $data[$language->locale] = ['name' => $input['name_' . $language->locale]];
            }
        }
        $phone_type = PhoneType::find($id);
        $phone_type->fill($data)->save();
        return redirect()->back()->with('success', 'Update was successfully done.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhoneType::destroy($id);
        return redirect()->back()->with('success', 'Phone type successfully removed!');

    }

    public function deleteTranslation($locale = '', $id)
    {
        if($locale && in_array($locale, config('translatable.locales'))) {
            if($locale == '' || !in_array($locale, config('translatable.locales'))) {
                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');
            }
            app()->setLocale($locale);
            $phone_type = PhoneType::find($id)->getTranslation($locale);
            $phone_type->delete();
            $current_language = Language::current();
            return redirect()->back()->with('success', 'Phone type for ' . $current_language->name . ' successfully removed!');
        }
        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');
    }
}
