<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\City;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
class ForgotPasswordController extends Controller
{
    public function __construct()
    {
       $this->general_settings = app('settings');

    }

    public function forgetPassword()
    {
      $countries = Country::All();
      return view('user.forgot_password')->with('general_settings', $this->general_settings)->with('countries',$countries);

    }
    public function forgotPasswordProcess(Request $request)
    {
        $input_data=$request->all();
        $this->validate($request, [
           'email' => 'required',
        ]);
        $user = User::where('email',$input_data['email'])->first();
        //print_r($user);echo count($user);exit;
        //Click here to reset your password: {{ url('password/reset/'.$token) }}
        if(count($user))
        {
            $enc_user = base64_encode($user->id.'####'.$user->email);
            $reset_pass_link = url('reset-password/'.$enc_user);

            $mailcontent = "Click here to reset your password: ".$reset_pass_link." ";
            $to=$input_data['email'];
            //dd($this->general_settings);
            // $headers ="From: ".$this->general_settings->SiteTitle." Admin <".$this->general_settings->Contact_Email.">\n";
            // $headers .= "MIME-Version: 1.0\n";
            // $headers .= "Content-type: text/html; charset=UTF-8\n";
            // $subject = $this->general_settings->SiteTitle.' Forget Password';
            // $message ="<html><head></head><body>"."<style type=\"text/css\">
            // <!--
            // .style4 {font-size: x-small}
            // -->
            // </style>
            // ".$mailcontent."
            // </body></html>";
            //
            // mail($to,$subject,$message,$headers);
             $data = [
                'user' => $user,
                'mailcontent' => $mailcontent,

            ];
            Mail::send('emails.forgot-password', $data, function ($message) use ($user,$input_data) {
                $message->from($this->general_settings->contact_email, $this->general_settings->site_title);
                $message->to($input_data['email'], $user->name)->subject('Forgot Password - '.$this->general_settings->site_title);
                $message->replyTo($this->general_settings->contact_email, $this->general_settings->contact_name);
            });


            return redirect(url('forget-password'))->with('forget_pass_success', 'Reset password link send successful!!');
        }
        else    return redirect(url('forget-password'))->with('error_msg', 'User credentials not found.');

    }

    public function resetPassword($token = '')
    {
        $countries = Country::All();
            return view('user.reset_password')->with('general_settings', $this->general_settings)->with('token', $token)->with('countries',$countries);

    }
    public function resetPassProcess(Request $request)
    {
        $input_data=$request->all();
        //dd($input_data);
        $this->validate($request, [
           'password' => 'required',
           'password_confirmation' => 'required|same:password',
        ]);
        if(!$input_data['token'])
        {
            return  redirect(url('reset-password/'.$input_data['token']))->with('error_msg', 'Invalid token error.');
        }
        if($input_data['password'] != $input_data['password_confirmation'])
        {
            return  redirect(url('reset-password/'.$input_data['token']))->with('error_msg', 'Password  confirmation password does not matched.');
        }
            $info_user = base64_decode($input_data['token']);
            $info_user_arr = explode('####',$info_user);

            $user_id = $info_user_arr[0];
            $update=0;
            $user = User::find($user_id);
            //print_r($user);exit;
            if(count($user)>0){
                $user->password = password_hash($input_data['password'], PASSWORD_DEFAULT);
                $update=$user->save();
                //
            }
            if($update==1){
                return  redirect(url('reset-password'))->with('reset_pass_success', 'Password successfully changed!');
            }

    }
}
