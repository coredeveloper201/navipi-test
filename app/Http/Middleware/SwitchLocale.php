<?php 
/*
|--------------------------------------------------------------------------
| Switch Locale
|--------------------------------------------------------------------------
|
| Here we switch language..
| Author SANTU ROY
| 
| 
*/
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;
use Closure;

class SwitchLocale {

/**
 * Handle an incoming request.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Closure  $next
 * @return mixed
 */
	
	public function handle($request, Closure $next)
	{		
		// get the last segment
		$ls = count($request->segments());
		//dd($request->segments());
		
		// Set the language
		if(in_array($request->segment($ls), config('translatable.locales')))
		{
			\Session::put('language_locale', $request->segment($ls));

			
			//return Redirect::to(substr($request->path(), 3));
			return Redirect::back();
		}

		// Check if the session has the language
		if(!\Session::has('language_locale')) {
			\Session::put('language_locale', config('app.fallback_locale'));
		}

		app()->setLocale(\Session::get('language_locale'));

		return $next($request);
	}

}