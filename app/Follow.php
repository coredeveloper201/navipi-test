<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Follow extends Model 
{
    protected $table = "follows";
    protected $fillable = [
        'followed_to', 'followed_by'
    ];
	
	public function followedByUser()
    {
        return $this->hasOne('App\User','id','followed_by');
    }
	public function followedToUser()
    {
        return $this->hasOne('App\User','id','followed_to');
    }
}
