<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class QuestionView extends Model 
{
	//
	 protected $table = "question_views";
	 protected $fillable = [
        'question_id','ip'
    ];
	
	public function question()
    {
        return $this->hasOne('App\AskQuestion','question_id','id');
    }
	
}
