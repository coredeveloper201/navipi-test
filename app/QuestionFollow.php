<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class QuestionFollow extends Model 
{
    protected $table = "question_follows";
    protected $fillable = [
        'question_id', 'followed_by'
    ];
	
	public function followedByUser()
    {
        return $this->hasOne('App\User','id','followed_by');
    }
	
}
