<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class FavoriteTropic extends Model 
{
	//favorite_tropic
	 protected $fillable = [
        'user_id','tropic_id'
    ];
	
	public function tropic()
    {
        return $this->hasOne('App\AllTropic','id','tropic_id');
    }
	
}
