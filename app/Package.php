<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Package extends Model 
{
	public $timestamps = false;
	
	protected $fillable = [
        'plan','title','duration','price','details',
    ];
		
	
}
