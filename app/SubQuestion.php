<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class SubQuestion extends Model 
{
	
	protected $fillable = [
        'question_id','sub_question'
    ];
		
	public function question()
    {
        return $this->belongsTo('App\Question');
    }
	
}
