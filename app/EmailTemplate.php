<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class EmailTemplate extends Model 
{
	public $timestamps = true;
	
	protected $fillable = [
        'email_title','description','locale'
    ];
		
	
}
