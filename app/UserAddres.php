<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class UserAddres extends Model 
{
    protected $table = "user_address";
    protected $fillable = [
        'user_id', 'address', 'is_default', 'type',
    ];
	
	
}
