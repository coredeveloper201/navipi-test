<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class BlogView extends Model 
{
	//
	 protected $table = "blog_views";
	 protected $fillable = [
        'blog_id','ip'
    ];
	
	public function blog()
    {
        return $this->hasOne('App\Blog','blog_id','id');
    }
	
}
