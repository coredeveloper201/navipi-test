<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class FavFriend extends Model 
{
	//
	 protected $table = "fav_friends";
	 protected $fillable = [
        'user_id','friend_id'
    ];
	
	public function friend()
    {
        return $this->hasOne('App\User','id','friend_id');
    }
	
}
