<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Education extends Model 
{
    protected $table = "educations";
    protected $fillable = [
        'user_id', 'institution', 'major',  'degree',  'city',  'country',  'from_month', 'from_year', 'to_month', 'to_year', 'status', 
    ];

	public function edu_degree()
    {
        return $this->hasOne('App\Degree','id','degree');
    } /* */
	
	
}
