<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class SubAnswer extends Model 
{
	public $timestamps = false;
	protected $fillable = [
        'question_id','correct_question','sub_answer','min_range','max_range'
    ];
		
	public function question()
    {
        return $this->belongsTo('App\Question');
    }
	
}
