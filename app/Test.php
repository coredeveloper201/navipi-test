<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Test extends Model 
{
	protected $fillable = [
        'title','user_id','start_time','end_time','attempt_type','attempt_count','show_guideline','time_limit','noof_guidelines','resume_later','show_points','randomize_question','must_answer_qsn','must_answer_correct','instant_grading','show_correct_answers','allow_go_back','status'
    ];
	
	public function questions()
    {
        return $this->belongsToMany('App\Question');
    }
	public function test_groups()
    {
        return $this->belongsToMany('App\Group');
    }
	
	public function test_evaluations()
    {
        return $this->hasMany('App\TestEvaluation','test_id','id')->orderBy('id','DESC');
    }	
	
}
