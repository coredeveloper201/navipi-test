<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class WorkExperience extends Model 
{
    protected $table = "work_experiences";
    protected $fillable = [
        'user_id', 'title', 'company',  'department',  'city',  'country',  'from_month', 'from_year', 'to_month', 'to_year', 'status', 
    ];

	/* public function work_experiences()
    {
        return $this->hasMany('App\WorkExperience','user_id','id')->orderBy('id','desc');
    } */
	
	
}
