<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Group extends Model 
{
	 protected $fillable = [
        'title','user_id',
    ];
	
	public function group_members()
    {
        return $this->belongsToMany('App\User')->orderBy('id','desc');
    }
	
	public function tests()
    {
        return $this->belongsToMany('App\Test')->orderBy('id','desc');
    }
}
