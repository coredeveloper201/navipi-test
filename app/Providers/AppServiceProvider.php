<?php

namespace App\Providers;

use App\Language;
use App\Setting;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->passCommonDataToEverywhere();
        $this->extendValidator();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
	
	protected function passCommonDataToEverywhere()
    {
        // Share settings
        $this->app->singleton('settings', function() {
            return Setting::first();
        });
        view()->share('settings', app('settings'));

        // Share languages
        $this->app->singleton('languages', function() {
            return Language::orderBy('weight', 'asc')->get();
        });
        view()->share('languages', app('languages'));
    }

    /**
     * Add custom validation rules
     */
    protected function extendValidator()
    {
        Validator::extend('old_password', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });
    }
	
}
