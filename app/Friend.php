<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Friend extends Model 
{
    protected $table = "friends";
    protected $fillable = [
        'from_user_id', 'to_user_id', 'status'
    ];
	
	public function form_user()
    {
        return $this->hasOne('App\User','id','from_user_id');
    }
	public function to_user()
    {
        return $this->hasOne('App\User','id','to_user_id');
    }
}
