<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('educations', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('institution',255);
            $table->string('major',255);
            $table->string('degree',255);
            $table->string('city',255);
            $table->integer('country')->unsigned()->index();
            $table->integer('from_month')->unsigned()->index();
            $table->integer('from_year')->unsigned()->index();
            $table->integer('to_month')->unsigned()->index();
            $table->integer('to_year')->unsigned()->index();
            $table->enum('status',['Y','N'])->default('Y')->comment = 'Y = Active, N = Inactive';			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('educations');
    }
}
