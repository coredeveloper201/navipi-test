<div class="hoverSpace">
	<div class="hoverBox clear">
        @if($user!==null)
		<div class="leftPop">
            <?php $profImag = asset('assets/frontend/images/profile.jpg');
            if($user!=null && $user->profile_image !='' && file_exists('assets/upload/profile_image/'.$user->profile_image)==1)
            {
                $profImag =asset('assets/upload/profile_image/'.$user->profile_image);
            }
            ?>
			<figure><img src="{!!$profImag!!}" alt=""></figure>
			<h2>{{get_user_name($user->id)}}</h2>
			<a href="{{url('chat/messages/'.$user->id )}}" class="bluebtn">message</a>
		</div>
		<div class="rightPop">
			<ul>
				<li><i class="fa fa-plus" aria-hidden="true"></i> Registered : {{date('d-m-Y',strtotime($user->created_at))}}</li>
				<li><i class="fa fa-heart" aria-hidden="true"></i> Age : {{$user->age}}</li>
				<li><i class="fa fa-transgender" aria-hidden="true"></i> Sex : {{$user->gender =="M" ?'Male':'Female'}}</li>
				<li><i class="fa fa-map-marker" aria-hidden="true"></i>
                    <?php $live = $user->user_Addresses()->where('is_default','Y')->first();?>
					@if(!empty($live))
						Country : {{$live->address}}
					@endif
				</li>
                <?php $we = $user->work_experiences()->orderBy('id','desc')->first();?>
				@if(!empty($we))
					<li><i class="fa fa-building-o" aria-hidden="true"></i> {{$we->company}}</li>
					<li><i class="fa fa-wrench" aria-hidden="true"></i> {{$we->title}}</li>
				@endif
			</ul>
		</div>
        @endif
	</div>
</div>