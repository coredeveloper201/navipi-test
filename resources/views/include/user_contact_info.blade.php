
<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
@if(count($user->contact_ifno)>0)
	@foreach($user->contact_ifno as $val)
	<form class="contactinfofrm">
		<div class="linkAddress viwSmlAdd">
			@if(Auth::check() && $user->id == Auth::user()->id) 
			<a href="javascript:void(0)" class="cnEdt rnvinfo"><img src="{!! asset('assets/frontend') !!}/images/cancel.png" alt=""></a>
			<a href="javascript:void(0)" class="edtEml"><img src="{!! asset('assets/frontend') !!}/images/edit.png" alt=""></a>
			@endif
			<ul>
				<li>
					@if($val->info1)
					<div class="textE clear"><a href="mailto:{{$val->info1}}"><img src="{!! asset('assets/frontend') !!}/images/gmail.png" alt=""> {{$val->info1}}</a></div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/gmail.png" alt=""><input name="info1" type="email" value="{{$val->info1}}"/>
					</div>
				</li>
				<li>
					@if($val->info2)
					<div class="textE clear"><a href="mailto:{{$val->info2}}"><img src="{!! asset('assets/frontend') !!}/images/message.png" alt=""> {{$val->info2}}</a></div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/message.png" alt=""><input name="info2" type="text" value="{{$val->info2}}"/>
					</div>
				</li>
				<li>
					@if($val->info3)
					<div class="textE clear"><a href="https://www.facebook.com/{{$val->info3}}"><img src="{!! asset('assets/frontend') !!}/images/fb1.png" alt=""> {{$val->info3}}</a></div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/fb1.png" alt=""><input name="info3" type="text" value="{{$val->info3}}"/>
					</div>
				</li>
				<li>
					@if($val->info4)
					<div class="textE clear"><a href="Tel:{{$val->info4}}"><img src="{!! asset('assets/frontend') !!}/images/call.png" alt=""> {{$val->info4}}</a></div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/call.png" alt=""><input name="info4" type="text" value="{{$val->info4}}"/>
					</div>
				</li>			
			</ul>
			<div class="textfld btnRow">
				<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
				<input type="submit" value="Save" class="bluebtn">
			</div>									
				<input type="hidden" name="info_id" value="{{$val->id}}"/>
		</div>
	</form>
	@endforeach
@endif