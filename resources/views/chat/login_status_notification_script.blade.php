@if(Auth::check())
	<script type="text/javascript">
		function updateLoginStatus()
		{
			$.ajax({
                type:"get",
                url: '{!! url('chat/updateLoginStatus') !!}',
                dataType: "json",
                success: function(response) {
                    
                }
            });
		}
		function updateNotification()
		{
			$.ajax({
                type:"get",
                url: '{!! url('chat/updateNotification') !!}',
                dataType: "json",
                success: function(response) {
                    if(response.has_error == 0)
                    {
                    	$('#notification_list_area').html(response.notification_html);
                        if(response.notification_arr['msg'] > 0)
                        {
                            $('#notification_msg_count1').html(response.notification_arr['msg']);
                            $('#notification_msg_count1').css('display','inline-block');
                        }
                        else
                        {
                            $('#notification_msg_count1').html('');
                            $('#notification_msg_count1').css('display','none');
                        }
                        if(response.notification_arr['flirt'] > 0)
                        {
                            $('#notification_flirt_count1').html(response.notification_arr['flirt']);
                            $('#notification_flirt_count1').css('display','inline-block');
                        }
                        else
                        {
                            $('#notification_flirt_count1').html('');
                            $('#notification_flirt_count1').css('display','none');
                        }
                        if(response.notification_arr['profile_view'] > 0)
                        {
                            $('#notification_view_profile_count').html(response.notification_arr['profile_view']);
                            $('#notification_view_profile_count').css('display','inline-block');
                        }
                        else
                        {
                            $('#notification_view_profile_count').html('');
                            $('#notification_view_profile_count').css('display','none');
                        }
                        if(response.notification_arr['favourite'] > 0)
                        {
                            $('#notification_fav_count1').html(response.notification_arr['favourite']);
                            $('#notification_fav_count1').css('display','inline-block');
                        }
                        else
                        {
                            $('#notification_fav_count1').html('');
                            $('#notification_fav_count1').css('display','none');
                        }
                    }
                    if(response.has_audio == 1)
                    {
                        $('#sound').html('<audio autoplay="autoplay"><source src="{!! asset('assets/frontend/audio/notification-in-site.mp3') !!}" type="audio/mpeg" /><source src="{!! asset('assets/frontend/audio/notification-in-site.ogg') !!}" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{!! asset('assets/frontend/audio/notification-in-site.mp3') !!}" /></audio>');
                    }
                }
            });
		}
		function unseenViewerCount()
		{
			$.ajax({
                type:"get",
                url: '{!! url('ajax/unseenViewerCount') !!}',
                dataType: "json",
                success: function(response) {
                    if(response.has_error == 0)
                    {
                    	$('#notification_view_profile_count').html(response.profile_viewer_count);
                    	$('#notification_view_profile_count').css('display','inline-block');

                        if(response.has_audio == 1)
                        {
                            $('#sound').html('<audio autoplay="autoplay"><source src="{!! asset('assets/frontend/audio/notify.mp3') !!}" type="audio/mpeg" /><source src="{!! asset('assets/frontend/audio/notify.ogg') !!}" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{!! asset('assets/frontend/audio/notify.mp3') !!}" /></audio>');
                        }
                    }
                    
                }
            });
		}
		$(document).ready(function(){
			updateNotification();
            unseenViewerCount();
			setInterval(function() {
				updateLoginStatus();
			}, 900000);
			setInterval(function() {
				updateNotification();
				unseenViewerCount();
			}, 4990);
		});
	</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('.popbox').fancybox({
             /*afterShow: function(){
                $(".popupwrap").mCustomScrollbar({
                    scrollButtons:{
                        enable:true
                    }
                });
            }*/
        });
        $(document).on('click','#notyfybtn',function() {
            $(this).next('.notifybox').slideToggle(0);
            
            $.ajax({
                type:"POST",
                url: '{!! url('chat/viewNotification') !!}',
                dataType: "json",
                success: function(response) {
                    $('#ntification_count').hide();
                }
            });
        });
    });
</script>