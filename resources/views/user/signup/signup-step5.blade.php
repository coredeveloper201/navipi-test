@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>							
				
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st green"><i class="fa fa-check"></i></span>
							<span class="st st2 green"><i class="fa fa-check"></i></span>
							<span class="st st3 green"><i class="fa fa-check"></i></span>
							<span class="st st4 green"><i class="fa fa-check"></i></span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current" style="width:75%;"></div>
							</div>
						</div>
					</div>
                    <div class="signupbox steplast clear">
                    	<div class="signupleft">
                        	<p style="font-size:25px"><strong>YOU ‘VE DONE IT!</strong></p>
                            <p>Thank you for letting us understand more about you.</p>
							<p>Now, you can start experiencing the website.</p>
							<p>We hope you enjoy it & together contribute to the community.</p>
                            <div class="buttonbox clear">
								<form name="signup" method="post" action="">   
								{{csrf_field()}}
									<a href="{{url('user/signup-step/4')}}" class="prevstep assbtn">prev</a>
									 <input type="submit" value="Finish" class="nextstep bluebtn">			
								 <input type="hidden" name="in_step" value="{{$step}}" />							
								</form>
							</div>
                        </div>
                        <div class="signupthumb clear">
                        	<img src="{!! asset('assets/frontend') !!}/images/step5.png" height="300" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->

@endsection

@section('customScript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKqvPeyXND6or2BXGG1O-qp-bC_lIz7Uk&libraries=places"></script>
<script>
$(document).ready(function() {
    $('.closebox').click(function() {
		$(this).parent('.addbox').parent('li').remove();
	});
});

</script>

@endsection