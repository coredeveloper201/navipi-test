<?php
//use App\Helpers\CustomHelper;
?>
@extends('layouts.basic')

@section('pageTitle', 'Hotcupido.com | Forgot Password')

@section('customStyle')

@endsection

@section('content')
    
<div class="middleadjstcont">    
    
<div class="popupDiv frgtpassform">
  <div class="popupDivHeader"> 
    <span class="popupTtl">Forgot Password</span>
    <div class="clear"></div>
  </div>
  <div class="popupDivBody">
  @if($errors->any())
  <div class="warnings">
      <strong>
          @foreach($errors->all() as $error)
          <p>{{ $error }}</p>
          @endforeach
      </strong>
  </div>
  @elseif(session('forget_pass_success'))
  <div class="success">
      <strong>{{ session('forget_pass_success') }}</strong>
  </div>
  @endif
  @if(session('error_msg'))
      <div class="warnings">
          {!! session('error_msg') !!}
      </div>
  @endif

    {!! csrf_field() !!}
    <form method="post" action="{{url('forget-password')}}" name="forgot_pass" data-parsley-validate>
    {!! csrf_field() !!}
      <div class="popupCont inline">
        <div class="logForm">
          <div class="formRow">
              <span class="formTtl">Registered Email <font>*</font></span>
              <span class="formFld">
                <input type="email" name="email" placeholder="Please put the registered email" value="{{ old('email') }}" required>
              </span>
          </div>          
        </div>
        <div class="clear"></div>
      </div>
      <div class="popupFooter">
        <button type="submit" class="full">Send Password Reset Link</button>
      </div>
    </form>
  </div>
</div>

</div>

@endsection

@section('customScript')

@endsection
