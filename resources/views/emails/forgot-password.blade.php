<!DOCTYPE html>
<html>
<head>
	<title>Forgot Password</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
img { -ms-interpolation-mode: bicubic; }
img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
table { border-collapse: collapse !important; }
body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }
div[style*="margin: 16px 0;"] { margin: 0 !important; }
</style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td bgcolor="#eeeeee" align="center" style="padding:10px;"><!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
      <?php $settings = app('settings');?>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="responsive-table">
        <tr>
          <td><!-- HERO IMAGE -->
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center"><img src="{!! asset('assets/frontend/images/hotcupidoemail-banner.jpg') !!}" width="100%" border="0" alt="{{$settings->site_title}}" style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;"></td>
              </tr>
              <tr>
                <td style="padding:15px 0;"><table width="100%">
                    <tr>
                      <td bgcolor="#ffffff" style="padding:20px;"><!-- COPY -->
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; padding:10px;">Dear {!!$user->username!!},</td>
                          </tr>
                          <tr>
                            <td style="padding:10px; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif;">{!! $mailcontent!!}</td>
                          </tr>
                       
                          <tr>
                            <td style="padding:10px; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif;">Sincerely<br>
                              Your Support Team</td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#2e323d" style="padding:15px; color:#fff; font-size:13px; font-family: Helvetica, Arial, sans-serif;">
                    <p style="color:#676767;"><strong>Contact Address:</strong> {{$settings->contact_address}}.</p>
                </td>
              </tr>
            </table></td>
        </tr>
      </table>
      
      <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]--></td>
  </tr>
</table>
</body>
</html>