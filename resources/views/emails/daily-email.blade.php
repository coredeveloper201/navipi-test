<?php
$setting = app('settings');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Newsletter</title>
<style>
*{
	margin:0;
	padding:0;
}
</style>
</head>

<body>

<table width="600px" cellpadding="0" cellspacing="0" style="margin:0 auto;">
	<thead>
        <tr>
            <th>
                <table cellpadding="0" cellspacing="0" style="margin:0; width:100%;">
                    <tr>
                        <td style="vertical-align:middle; padding:15px; background:#000; text-align:left;"><a href="" style="outline:none; float:left;"><img src="{!! asset('assets/upload/site_logo/'.$setting->site_logo) !!}" height="35px" alt="" style="display:block;"></a></td>
                        <td style="vertical-align:middle; padding:15px; background:#000; text-align:right;"></td>
                    </tr>
                </table>
            </th>
        </tr>
    </thead>
    
    <tbody>
    	<tr>
        	<td>
            	<table cellpadding="0" cellspacing="0" style="margin:0; width:100%;">
                	<tr>
                    	<td style="padding:15px 15px 15px 0px; width:209px; vertical-align:middle;">
                            @if($model->prifile_picture != '' && file_exists('assets/upload/profile_pictures/'.$model->prifile_picture))
                                <img src="{!! asset('assets/upload/profile_pictures/'.$model->prifile_picture) !!}" alt="{!! $model->username !!}" width="209" height="209" style="display:block;"/>
                            @else
                                <img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $model->username !!}" width="209" height="209" style="display:block;"/>
                            @endif
                        </td>
                        <td style="padding:15px 0px 15px 15px; vertical-align:middle;">
                        	<p style="margin:0 0 20px; font-family: Arial, sans-serif; font-size:18px;"><strong style="font-weight:700; margin-right:12px; color:#ee4c68;">Name</strong><span>{!! $model->username!!} | {!! floor((time() - strtotime($model->dob))/(365 * 24 * 60 * 60)) !!}</span></p>
                            <a href="{!! url('user/profile-details/'.$model->id) !!}" target="_blank" style="text-transform:capitalize; font-family: Arial, sans-serif; display:inline-block; color:#fff; line-height: 1.2; padding: 12px 30px; border-bottom: 2px solid #e21c3f; border-radius: 4px; background-color: #ee4c68; font-weight: 400;">Click here to start a chat</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        {{-- <tr>
        	<td style="padding:15px 15px 30px 15px; text-align:center;">
            	<a href="" style="font-family: Arial, sans-serif; font-size:14px; color:#ee4c68;">http://..... ..... ..... ..\.. .... ......... ..... ..\... .... .... ....</a>
            </td>
        </tr> --}}
    </tbody>
    
    <tfoot>
        <tr>
            <td style="background:#000; padding:20px 15px; text-align:center; font-family: Arial, sans-serif; font-size:14px; font-weight:400; color:#e0e0e0; font-size:13px;">
                <p style="padding-bottom:15px; font-size:15px;">If you do not wish to receive emails about new potential partners
                you can easily cancel them here: Unsubscribe</p>
                <p style="color:#676767;"><strong>Head Office:</strong> Kikk Media Global LTD, Strovolou, 77 STROVOLOS CENTRE, <br>2nd floor, Flat/Office 204, Strovolos 2018, Nicosia, Cyprus.</p>
            </td>
        </tr>
    </tfoot>
</table>
<!--font-family: Arial, sans-serif;-->
</body>
</html>
