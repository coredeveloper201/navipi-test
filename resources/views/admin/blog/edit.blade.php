<?php

/**

 * Page resource built upon CoC

 * You can leave this as it is

 * or feel free to remove these configuration and customize


 */



$resource = 'blog';

$resource_pl = str_plural($resource);

$page_title = ucfirst(str_replace('_', ' ', $resource));

$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));

// Resolute the data

$data = ${$resource};


?>



@extends('admin.layouts.app')



@section('pageTitle', $page_title)



@section('content')

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Blog

            <small>Edit {!! $data->title !!}</small>

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i>Blog</a></li>

            <li class="active">Edit Blog</li>

        </ol>

    </section>

    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-header with-border">

                        <h3 class="box-title">Edit Blog</h3>

                    </div>

                    @if($errors->any())

                        <div class="alert alert-danger">

                            @foreach($errors->all() as $error)

                                <p>{!! $error !!}</p>

                            @endforeach

                        </div>

                    @endif

                    @if(session('success'))

                    <div class="alert alert-success">

                        {!! session('success') !!}

                    </div>

                    @endif

                    <form method="POST" action="{!! admin_url($resource . '/' . ${$resource}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                        <input name="_method" type="hidden" value="PATCH">{{ csrf_field() }}

                        <div class="box-body">								
 
                           <div class="form-group">	
								<label for="title" class="col-sm-2 control-label">Title *</label>	
								<div class="col-sm-6">	
								<input type="text" required class="form-control" id="title" name="title" placeholder="title" value="{!! $data->title !!}" />	
								</div>		
							</div>	
							@if($data->cat_id!=0)
							<div class="form-group">	
								<label for="category" class="col-sm-2 control-label">Category </label>	
								<div class="col-sm-6">	
									<span class="form-control" style="border:none;">{{$data->blog_category->title}}</span>
								</div>		
							</div>	
							@endif
							<div class="form-group">
								<label for="blog_image" class="col-sm-2 control-label">Image</label>
								<div class="col-sm-6">
									<span class="btn btn-default btn-file">
										Browse <input type="file"  id="blog_image" name="blog_image" />
									</span>
									<p class="help-block" id="thumb_image_help">Blog Image</p>
									<img class="list_table_img" src="{!! asset('assets/upload/blog_image/'.$data->blog_image) !!}" alt="No Image">
								</div>
							</div>								
							<div class="form-group">	
								<label for="description" class="col-sm-2 control-label">Description *</label>	
								<div class="col-sm-6">	
								<textarea required class="form-control ckeditor" id="description" name="description" placeholder="description">{!! $data->description !!}</textarea>	
								</div>		
							</div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">

                            <a class="btn btn-default" href="{!! url("admin/$resource") !!}">

                                Back</a>

                            <button type="submit" class="btn btn-info pull-right">Save</button>

                        </div><!-- /.box-footer -->

                        </form>

                </div><!-- /.box -->

            </div>

        </div>

    </section>

    <!-- /.content -->

</div><!-- /.content-wrapper -->

@endsection



@section('customScript')

<script>



    $(function () {

		$('#group_id').select2({

            placeholder: "Select Group"

            //allowClear: true

        });

    });





</script>
<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>


@endsection