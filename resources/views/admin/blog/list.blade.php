<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'blog';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title_pl . ' list')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blog
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Blog</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Blog list</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>								
								<th><input type="checkbox"  id="bulkDelete"  /> <button id="deleteTriger">Bulk Delete</button></th>
                                <th>Blog</th>
								@if(${$resource_pl}[0]->user_id !=0)
                                <th>Category</th>
								@endif
                                <th>Added By</th>
                                <th>Image</th>
                                <th>Post On</th>
                                <th style="width:300px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(${$resource_pl} !== null)
                                @foreach(${$resource_pl} as $ky=>$row)
                                    <tr>							
										<td><input type="checkbox"  class="deleteRow" value="{{$row->id}}"  /> #{!!$ky+1!!} </td>
                                        <td>{!! $row->title !!}</td>
										@if($row->user_id !=0)
                                        <td>{!! isset($row->blog_category)?$row->blog_category->title:'Other' !!}</td>
										@endif
                                        <td>{!! ($row->user_id != 0)?$row->blog_user->first_name.' '.$row->blog_user->last_name:'Admin' !!}</td>
                                        <td><img class="list_table_img" src="{!! asset('assets/upload/blog_image/'.$row->blog_image) !!}" alt="No icon"></td>
                                        <td>{!! $row->created_at !!}</td>
                                        <td>
                                            <a href="{!! admin_url($resource . '/' . $row->id . '/edit') !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
											
											@if($row->user_id !=0)
                                            <form method="POST" action="{!! admin_url('blog/block') !!}"

                                                  onsubmit="return confirm('Are you sure to {{$row->status=='Y'?'block':'unblock'}} {!! $row->first_name !!}?');">

                                                <input name="id" type="hidden" value="{{$row->id}}">{{ csrf_field() }}

                                                <button class="btn btn-sm btn-{{$row->status=='Y'?'success':'danger'}} td-btn" type="submit"><i class="fa fa-ban" aria-hidden="true"></i> {{$row->status=='Y'?'Approved':'Not Approved'}}</button>

                                            </form>
											@endif
                                            <form method="POST" action="{!! admin_url($resource . '/' . $row->id) !!}"
                                                  onsubmit="return confirm('Are you sure to remove {!! $row->first_name !!}?');">
                                                <input name="_method" type="hidden" value="DELETE"/>{{ csrf_field() }}
                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No Car maker Found..</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        {!! ${$resource_pl}->render() !!}
                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
		
		$("#bulkDelete").on('click',function() { // bulk checked
			var status = this.checked;
			$(".deleteRow").each( function() {
				$(this).prop("checked",status);
			});
		});
		 
		$('#deleteTriger').on("click", function(event){ // triggering delete one by one
			if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
				var ids = [];
				$('.deleteRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var ids_string = ids.toString();  // array to string conversion 
				$.ajax({
					type: "POST",
					headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
					url: "{{url('admin/blog-delete-all')}}",
					data: {data_ids:ids_string},
					success: function(result) {
						//dataTable.draw(); // redrawing datatable
						location.reload();
					},
					async:false
				});
			}
		});
		
    });
</script>
@endsection