<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$resource = 'tropic';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
$locale = app()->getLocale();

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Topic
            <small>Import/Export</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url('admin/'.$resource) !!}"><i class="fa  fa-user"></i> Topics</a></li>
            <li class="active"> Import/Export </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import/Export Topic CSV</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success">
                            {!! session('success') !!}
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">					
                           <p>{!! session('error') !!}</p>								
                        </div>
                    @endif
                    <form method="POST" action="" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">							
                            <div class="form-group">	
								<label for="title" class="col-sm-2 control-label">Import Topic CSV*</label>	
								<div class="col-sm-6">	
								
									Browse <input type="file"  id="" name="topic_csv" />
								</div>									<span class="btn btn-default btn-file">
								<a href="{!! asset('assets/upload/topic_csv/samplecsv.csv') !!}" download >Donload Sample</a>								</span> <!-- -->						
							</div>							
                            <div class="form-group">	
								<label for="title" class="col-sm-2 control-label">Export All Topic </label>									<span class="btn btn-default btn-file">
								<a href="{!! asset('assets/upload/topic_csv/tropics.csv') !!}" download >Donload Tropics Csv</a>								</span> <!-- -->						
							</div>	
							
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url('admin/$resource') !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')

<script type="text/javascript">

    $(function () {
		
    });

</script>

@endsection