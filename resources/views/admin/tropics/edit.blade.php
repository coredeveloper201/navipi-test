<?php

/**

 * Page resource built upon CoC

 * You can leave this as it is

 * or feel free to remove these configuration and customize

 * @author Tuhin | <tuhin@technoexponent.com>

 */



$resource = 'tropic';

$resource_pl = str_plural($resource);

$page_title = ucfirst(str_replace('_', ' ', $resource));

$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));

// Resolute the data

$data = ${$resource};



?>



@extends('admin.layouts.app')



@section('pageTitle', $page_title)



@section('content')

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Topic

            <small>Edit {!! $data->name !!}</small>

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i>Topics</a></li>

            <li class="active">Edit Topic</li>

        </ol>

    </section>

    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-header with-border">

                        <h3 class="box-title">Edit Topic</h3>

                    </div>

                    @if($errors->any())

                        <div class="alert alert-danger">

                            @foreach($errors->all() as $error)

                                <p>{!! $error !!}</p>

                            @endforeach

                        </div>

                    @endif

                    @if(session('success'))

                    <div class="alert alert-success">

                        {!! session('success') !!}

                    </div>

                    @endif

                    <form method="POST" action="{!! admin_url($resource . '/' . ${$resource}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                        <input name="_method" type="hidden" value="PATCH"> {{ csrf_field() }}

                        <div class="box-body">								

                            <div class="form-group">
                                <label for="plan" class="col-sm-2 control-label">Topic * </label>
								<div class="col-sm-6">
									<input type="text" required class="form-control" name="title" placeholder="title" value="{!! $data->title !!}" />
								</div>	
                            </div>		
							<div class="form-group">

                                <label for="group_id" class="col-sm-2 control-label">Topic Group *</label>

                                <div class="col-sm-6">

									<select required class="form-control" id="group_id" name="group_id">
									@if(!empty($groups))
										@foreach($groups as $grp)
										<option type="text" class="form-control" value="{{$grp->id}}" {{ $data->group_id == $grp->id ? 'selected' : '' }} > {{$grp->name}}</option>
										@endforeach
									@endif
										<option type="text" class="form-control" value="0" {{ $data->group_id == 0 ? 'selected' : '' }} > Others</option>
									</select>

                                </div>								

                            </div>	
							<div class="form-group">
								<label for="image" class="col-sm-2 control-label">Image</label>
								<div class="col-sm-6">
								<span class="btn btn-default btn-file">
									Browse <input type="file" id="image" name="image" />
								</span>
									<img class="list_table_img" src="{!! asset('assets/upload/topic/'.$data->image) !!}" alt="No Logo">
								</div>
							</div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">

                            <a class="btn btn-default" href="{!! url("admin/$resource") !!}">

                                Back</a>

                            <button type="submit" class="btn btn-info pull-right">Save</button>

                        </div><!-- /.box-footer -->

                        </form>

                </div><!-- /.box -->

            </div>

        </div>

    </section>

    <!-- /.content -->

</div><!-- /.content-wrapper -->

@endsection



@section('customScript')

<script>



    $(function () {

		$('#group_id').select2({

            placeholder: "Select Group"

            //allowClear: true

        });

    });





</script>



@endsection