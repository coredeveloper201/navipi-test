<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'invoice';
$resource_pl = 'invoices';
$page_title = ucfirst($resource_pl);
?>

@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $page_title }}
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">{{ $page_title }}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All {{ $resource }} list</h3>
						<a style="float:right !important" href="{{admin_url('user/'.$user->id.'/add-invoice')}}" class="btn btn-sm btn-warning td-btn">Add {{ $resource }}</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Plan</th>
                                <th>Amount</th>
                                <th>Invoice Date</th>
                                <th>Expired On </th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(${$resource_pl} !== null)
                                @foreach($invoices as $row)
								<?php 
								if(!empty($row))
								{
									$startd = strtotime($row->pivot->created_at);
									$exp = ($row->duration * (30)*86400);
									$expd = (int)$startd + $exp;
								}
								?>
                                    <tr>
                                        <td>{!! $row->plan!!}</td>
                                        <td>${!! $row->price !!}</td>
                                        <td>{{date('d M,Y',$startd)}}</td>
                                        <td>{{date('d M,Y',$expd)}}</td>
                                        <td>{{$row->pivot->valid == 'Y'?'Current':'Expired'}}</td>
                                        <?php
                                        /*<td>
                                            <select  class="status">
                                                <option data-id="{{ $row->id }}"  class="bg-green-active color-palette" value="Y" {{ $row->status == 'Y'?'selected':''  }}>Active</option>
                                                <option data-id="{{ $row->id }}" class="bg-red-active color-palette" value="N" {{ $row->status == 'N'?'selected':''  }}>Block</option>
                                                <option  data-id="{{ $row->id }}" class="bg-orange-active color-palette" value="E" {{ $row->status == 'E'?'selected':''  }}>Pending</option>
                                            </select>
                                        </td> */ ?>
                                        <td>
										<a href="{{admin_url('user/'.$row->pivot->user_id.'/invoice-details/'.$row->pivot->id)}}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No user Found..</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <!-- <div class="paginationDiv">
					{{${$resource_pl}->render() } }
                    </div> -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
    });
</script>
@endsection