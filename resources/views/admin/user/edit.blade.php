<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'user';
$resource_pl = str_plural($resource);
$page_title = ucfirst($resource);
// Resolute the data
$data = ${$resource};

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $page_title !!}
            <small>Edit {!! $data->firstname !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i> {!! ucfirst($resource_pl) !!}</a></li>
            <li class="active">Edit User</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                    <div class="alert alert-success">
                        {!! session('success') !!}
                    </div>
                    @endif
                    <form method="POST" action="{!! admin_url('user/' . ${$resource}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{!! $data->first_name !!}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{!! $data->last_name !!}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{!! $data->email !!}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{!! $data->address !!}" />
                                </div>
                            </div>


                            <?php /*
                            <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input type="radio" name="status" id="optionsRadios1" value="Y" {!! $data->status=='Y'?'checked':'' !!} >
                                        Active
                                    </label>
                                    <label>
                                        <input type="radio" name="status" id="optionsRadios2" value="N" {!! $data->status=='N'?'checked':'' !!} />
                                        Block
                                    </label>
                                    @if($data->status=='E')
                                        <label>
                                            <input type="radio" name="status" id="optionsRadios3" value="E" {!! $data->status=='E'?'checked':'' !!} />
                                            Pending Activation
                                        </label>
                                    @endif
                                </div>
                            </div>
                            */ ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url("admin/$resource") !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')

@endsection